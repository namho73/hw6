﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;

namespace hw6
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            CreateCharts();
        }

        private void CreateCharts()
        {
            
            var entryList = new List<Entry>()
            {
                 new Entry(600)
                {
                    Label = "German Shepherd",
                    Color = SKColor.Parse("#266489"),
                },
                new Entry(400)
                {
                    Label = "Husky",
                    Color = SKColor.Parse("#68B9C0"),
                },
                new Entry(300)
                {
                    Label = "Pitbull",
                    Color = SKColor.Parse("#90D585"),
                }
            };
            var barChart = new RadialGaugeChart() { Entries = entryList };
            var donutChart = new DonutChart() { Entries = entryList };
            var radarChart = new RadarChart() { Entries = entryList };

            MainPicker.Items.Add("Radial Gauge Chart");
            MainPicker.Items.Add("Donut Chart");
            MainPicker.Items.Add("Radar Chart");

            Chart1.Chart = barChart;
            Chart2.Chart = donutChart;
            Chart3.Chart = radarChart;
            Chart2.IsVisible = false;
            Chart3.IsVisible = false;

        }


        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Chart1.IsVisible = !Chart1.IsVisible;
        }

        private void OnPickerChanged(object sender, EventArgs e)
        {
            var selectedChart = MainPicker.Items[MainPicker.SelectedIndex];
            Chart1.IsVisible = false;
            Chart2.IsVisible = false;
            Chart3.IsVisible = false;

            if (selectedChart == "Radial Gauge Chart")
            {
                Chart1.IsVisible = true;
            } else if (selectedChart == "Donut Chart")
            {
                Chart2.IsVisible = true;
            } else if (selectedChart == "Radar Chart")
            {
                Chart3.IsVisible = true;
            }
                
        }
    }
}
